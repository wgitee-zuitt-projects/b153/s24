//MONGODB QUERY OPERATORS & FIELD PROJECTION


//$set is a MongoDB query operator that replaces the value of a field with the specified value.

//If a given field does not yet exist in the record, $set will add a new field with the specified value.

db.users.updateOne(
		{
			_id: ObjectId("61fbcab1fb5fca1da88b0b2e")
		},
		{
			$set: {
				skills: []
			}
		}
	)

//add (a single item) to an existing array - $push

db.users.updateOne(
		{
			_id: ObjectId("61fbcab1fb5fca1da88b0b2e")
		},
		{
			$push: {
				skills: "HTML"
			}
		}
	)

//add multiple elements to an existing array at the same time - we use $push & $each

db.users.updateOne(
		{
			_id: ObjectId("61fbcab1fb5fca1da88b0b2e")
		},
		{
			$push: {
				skills: {
					$each: ["CSS", "JavaScript"]
				}
			}
		}
	)

// delete an existing field - $unset

db.users.updateOne(
		{
			_id: ObjectId("61fbcab1fb5fca1da88b0b2e")
		},
		{
			$unset: {
				skills: ""
			}
		}
	)
//Mini-Activity: creating multiple users with different fields(name, age, isActive, skills)

db.users.insertMany([
		{
			name: "J.Edgar Hoover",
			age: 74,
			isActive: false,
			skills: [
				
					"HTML",
					"CSS",
					"Bootstrap"
				
			]
		},
		{
			name: "Robin Locksley",
			age: 36,
			isActive: true,
			skills: [
				
					"HTML",
					"CSS",
					"JavaScript"
				
			]
		},
		{
			name: "Count Dracula",
			age: 27,
			isActive: true,
			skills: [
				
					"HTML",
					"MERN"
				
			]
		},
		{
			name: "Leonardo DaVinci",
			age: 31,
			isActive: true,
			skills: [
				
					"HTML",
					"PHP",
					"MySQL"
				
			]
		},
		{
			name: "Charles Babbage",
			age: 80,
			isActive: false,
			skills: [
				
					"JavaScript",
					"Python"
			]
		}
		
	])

//search for users that know either JavaScript or MERN - $in (for OR operator)

db.users.find({
		skills: {
			$in: ["JavaScript", "MERN"]
		}
	})

// search for users that know HTML AND CSS - $all (for AND operator)

db.users.find({
	skills: {
		$all: ["HTML", "CSS"]
	}
})


//Field Projection: - show listings that are of property type "condominium," has a minimum stay of 1 night, has 1 bedroom, and can accommodate 2 people.

db.listingsAndReviews.find(
		{
			property_type: "Condominium",
			minimum_nights: "2",
			bedrooms: 1,
			accommodates: 2
		},
		{
			name: 1,
			property_type: 1,
			minimum_nights: 1,
			bedrooms: 1,
			accommodates: 1
		}

	)
.limit(5)

// .limit() - limits the results to the specified number

//if you want to show everything else except with what you want to see, we can use 0. That means we won't see [property type, minimum_nights, bedrooms & accommodates]

db.listingsAndReviews.find(
		{
			property_type: "Condominium",
			minimum_nights: "2",
			bedrooms: 1,
			accommodates: 2
		},
		{
			name: 0,
			
		}

	)

// Show listings that have a price of less than 100, fee for additional heads that is less than 20, and are in the country Portugal 

db.listingsAndReviews.find(
		{
			price: {
				$lt: 100 // $lt is less than
			},
			extra_people: {
				$lt: 20
			},
			"address.country": "Portugal"
		},
		{
			name: 1,
			price: 1,
			extra_people: 1,
			"address.country": 1	
		}
	)
.limit(5)

// search for the first 5 listings by name

db.listingsAndReviews.find(
		{
			price: {
				$lt: 100
			}
		},
			
		{
			name: 1,
			price: 1,
				
		}
	)
.limit(5)
.sort({
	name: 1 // sorts in an ascending order
})

//use -1 for descending

/*{
    "_id" : "11012484",
    "name" : "",
    "price" : NumberDecimal("40.00")
}

/* 2 
{
    "_id" : "22210558",
    "name" : "\"Hôtel\" room Frontenac metro, private & furnished",
    "price" : NumberDecimal("40.00")
}

/* 3 
{
    "_id" : "21568012",
    "name" : "$29 per night, near city,free wifi, own room,WOW!!",
    "price" : NumberDecimal("45.00")
}

/* 4 
{
    "_id" : "20358824",
    "name" : "$45ANYCCozy Room with curtain NearJ,G, and M train",
    "price" : NumberDecimal("45.00")
}

/* 5 
{
    "_id" : "21675318",
    "name" : "( ͡° ͜ʖ ͡°) Baaasic Spaaace (ฅ´ω`ฅ)",
    "price" : NumberDecimal("79.00")
}*/



// increase the price of the previous 5 listings by 10

db.listingsAndReviews.updateMany(
		{
			price: {
				$lt: 100
			}
		},
		{
			$inc: {           // $inc increament
				price: 10
			}
		}
	)

//to decrease, make the value a negative e.g. $inc: {price: -10}

db.listingsAndReviews.updateMany(
		{
			price: {
				$lt: 100
			}
		},
		{
			$inc: {     
				price: -10
			}
		}
	)

// delete listings with any one of the following conditions:
// review scores accuracy <=75
// review scores cleanliness <= 80
// review scores checkin <= 70
// review scores communication <= 70
// review scores location <= 75
// review scores value <= 80

db.listingsAndReviews.deleteMany(
  {
    $or:
    [ 
      {"review_scores.review_scores_accuracy": {$lte: 75}},
      {"review_scores.review_scores_cleanliness": {$lte: 80}},
      {"review_scores.review_scores_checkin": {$lte: 70}},
      {"review_scores.review_scores_communication": {$lte: 70}},
      {"review_scores.review_scores_location": {$lte: 75}},
      {"review_scores.review_scores_value": {$lte: 80}}
    ]
  }
)

//$lte - stands for less than or equal to

//$gt and $gte can be used to greater than and greater than or equal to